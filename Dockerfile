FROM python:3.11.0-alpine3.16

ARG VERSION
ENV VERSION ${VERSION:-3.1.8}
ARG PYDEPS
ENV PYDEPS ${PYDEPS}

# Update system
RUN apk update && apk --no-cache upgrade

# Install radicale + dependencies
RUN apk add --no-cache git openssh tzdata \
    && pip install --upgrade pip \
    && pip install pytz passlib[bcrypt] pyzmq $PYDEPS \
    && pip install radicale==$VERSION

# Create user/group account
RUN addgroup -g 1000 radicale \
    && adduser -D -s /bin/false -H -u 1000 -G radicale radicale

# Setup radicale directories
RUN mkdir -p /etc/radicale /var/lib/radicale \
    && chown -R radicale:radicale /etc/radicale && chmod 750 -R /etc/radicale \
    && chown -R radicale:radicale /var/lib/radicale && chmod 750 -R /var/lib/radicale

# Cleaning actions
RUN rm -fr /root/.cache

USER radicale

# Installing files
VOLUME /etc/radicale /var/lib/radicale
COPY config /etc/radicale/config

ENTRYPOINT [ "python3", "-m", "radicale", "--config" ]
CMD ["/etc/radicale/config"]
